This executable subscribes to a few sensors and prints out a rolling buffer of results

It deliberately uses the native DBus API so we can test parts individually

<!--
SPDX-FileCopyrightText: 2020 David Edmundson <davidedmundson@kde.org>
SPDX-License-Identifier: BSD-3-Clause
-->
